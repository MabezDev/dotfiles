[Appearance]
BoldIntense=false
ColorScheme=monokai
Font=Space Mono,14,-1,0,50,0,0,0,0,0
UseFontLineChararacters=true

[General]
Command=/usr/bin/zsh
LocalTabTitleFormat=
Name=Default
Parent=FALLBACK/
RemoteTabTitleFormat=

[Scrolling]
ScrollBarPosition=2
